/**
 * Init
 */
var canvas = document.getElementsByClassName('canvas')[0];

window.onresize = function () {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
};

window.onresize();

var ctx = canvas.getContext('2d');

ctx.font = 'bold 200px "Arial"';
ctx.textBaseline = 'center';
ctx.fillStyle = 'yellow';

var particles = [];
var particlesLength = 0;

var currentText = "Meow...";

window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

/**
 * Check if pixel has alpha
 * @param pixels
 * @param i
 * @returns {boolean}
 */
var checkAlpha = function checkAlpha(pixels, i) {
    return pixels[i * 4 + 3] > 0;
};

/**
 * Create particles
 */
var createParticles = function createParticles() {
    var textSize = ctx.measureText(currentText);
    ctx.fillText(currentText, ~~((canvas.width / 2) - (textSize.width / 2)), ~~(canvas.height / 2));

    var imageData = ctx.getImageData(1, 1, canvas.width, canvas.height);
    var pixels = imageData.data;
    var dataLength = imageData.width * imageData.height;

    //Loop through all pixels
    for (var i = 0; i < dataLength; i++) {
        var currentRow = Math.floor(i / imageData.width);
        var currentColumn = i - Math.floor(i / imageData.height);

        if (currentRow % 3 || currentColumn % 3) {
            continue;
        }

        //If alpha channel is greater than 0
        if (checkAlpha(pixels, i)) {
            var cy = ~~(i / imageData.width);
            var cx = ~~(i - (cy * imageData.width));

            createParticle(cx, cy);
        }
    }

    particlesLength = particles.length;
};

/**
 * Create one particle
 * @param x
 * @param y
 */
var createParticle = function createParticle(x, y) {
    particles.push(new Particle(x, y));
};

/**
 * Particle constructor
 * @param x
 * @param y
 * @constructor
 */
var Particle = function Particle(x, y) {
    this.startX = x;
    this.startY = y;

    /**
     * Movement variables
     */
    var vx,
        vy,
        ax,
        ay,
        frict;

    /**
     * Reset particle
     */
    this.reset = function reset() {
        this.x = this.startX;
        this.y = this.startY;

        this.color = "yellow";
        this.startLife = this.life = 5 + (random() * 10);

        this.len = 0;

        vx = 0;
        vy = 0;
        ax = 0;
        ay = -(random() * 0.4) - 0.2;
        frict = 0.75;
    };

    /**
     * Particle tick
     */
    this.tick = function tick() {
        this.physics();
        this.checkLife();
        this.checkColor();

        this.draw();
    };

    /**
     * Draw particle
     */
    this.draw = function draw() {
        ctx.fillStyle = this.color;

        ctx.fillRect(this.x, this.y - this.len, 0.5, this.len);
    };

    /**
     * Calculate particle movement
     */
    this.physics = function physics() {
        vx += ax;
        vy += ay;

        vx *= frict;
        vy *= frict;

        ax = (random() - 0.5) * 0.2;

        this.x += vx;
        this.y += vy;
    };

    /**
     * Calculate color
     */
    this.checkColor = function checkColor() {
        this.color = "hsla(" + ~~((this.life / 100) * 60 + 60) + ", 80%, 50%," + (~~(this.life / this.startLife * 100) / 100) + ")";
    };
    /**
     * Calculate opacity
     */
    this.checkLife = function checkLife() {
        this.life -= 0.5;

        if (this.life < 1) {
            this.reset();
        }

        this.len += (random() * 2) - 0.5;
    };

    /**
     * First init reset
     */
    this.reset();
};

/**
 * Main animation loop
 */
var animLoop = function animLoop() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < particlesLength; i++) {
        particles[i].tick();
    }

    requestAnimationFrame(animLoop);
}();

/**
 * Start the party
 */
createParticles();
